
/** ------------------------------------------------------
 * THIS FILE WAS AUTOMATICALLY GENERATED (DO NOT MODIFY)
 * -------------------------------------------------------
 */

/* tslint:disable */
/* eslint-disable */
export enum Gender {
    GIRL = "GIRL",
    BOY = "BOY"
}

export enum Language {
    HU = "HU"
}

export enum Round {
    ONE = "ONE",
    TWO = "TWO"
}

export enum Score {
    NO_WAY = "NO_WAY",
    WHY_NOT = "WHY_NOT",
    ONE = "ONE",
    TWO = "TWO",
    THREE = "THREE",
    FOUR = "FOUR",
    FIVE = "FIVE"
}

export interface SignupInput {
    mother: CreateUserInput;
    father: CreateUserInput;
    child: CreateChildInput;
}

export interface CreateChildInput {
    title: string;
    lastName: string;
    gender: Gender;
    language: Language;
}

export interface CreateUserInput {
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    email: string;
}

export interface VoteInput {
    childId: string;
    nameId: string;
    score: Score;
}

export interface IMutation {
    signup(input: SignupInput): string | Promise<string>;
    createChild(input: CreateChildInput): string | Promise<string>;
    createUser(input: CreateUserInput): string | Promise<string>;
    vote(input: VoteInput): string | Promise<string>;
}

export interface Child {
    id: string;
    title: string;
    lastName: string;
    gender: Gender;
    family: Family;
    language: Language;
}

export interface IQuery {
    children(): Child[] | Promise<Child[]>;
    randomName(childId: string, round?: Round): RandomName | Promise<RandomName>;
    stats(childId: string): Stats | Promise<Stats>;
    familyStats(childId: string): FamilyStats | Promise<FamilyStats>;
    user(id: string): User | Promise<User>;
    users(): User[] | Promise<User[]>;
    votes(childId: string): Vote[] | Promise<Vote[]>;
}

export interface Family {
    id: string;
    mother: User;
    father: User;
}

export interface Name {
    id: string;
    gender: Gender;
    language: Language;
    name: string;
}

export interface RandomName {
    name: Name;
    round: Round;
}

export interface Stats {
    userId: string;
    childId: string;
    allNamesCount: number;
    noWayCount: number;
    whyNotCount: number;
    rankedCount: number;
    leftCount: number;
    progress: number;
}

export interface FamilyStats {
    self: Stats;
    partner: Stats;
}

export interface ISubscription {
    familyStatsUpdated(childId: string): FamilyStats | Promise<FamilyStats>;
}

export interface User {
    id: string;
    username: string;
    firstName: string;
    lastName: string;
    email: string;
}

export interface Vote {
    id: string;
    user: User;
    child: Child;
    name: Name;
    score?: Score;
}
