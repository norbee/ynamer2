export interface JwtPayload {
  uid: string;
  sub: string;
  username: string;
  exp?: number;
}
