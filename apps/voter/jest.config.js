module.exports = {
  name: 'voter',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/voter',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
