import { EventEmitter } from 'events';
import { StrictEventEmitter } from 'nest-emitter';

import { VoterEvents } from '@ynamer/api/voter';

export type AppEventEmitter = StrictEventEmitter<EventEmitter, VoterEvents>;
