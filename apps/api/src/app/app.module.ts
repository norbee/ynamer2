import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from '@ynamer/api/auth';
import authConfig from '@ynamer/api/config/auth';
import dbConfig from '@ynamer/api/config/database';
import { DatabaseModule } from '@ynamer/api/database';
import { GraphqlModule } from '@ynamer/api/graphql';
import { ImporterModule } from '@ynamer/api/importer';
import { UserModule } from '@ynamer/api/user';
import { VoterModule } from '@ynamer/api/voter';
import { EventEmitter } from 'events';
import { NestEmitterModule } from 'nest-emitter';
import { join } from 'path';

import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [authConfig, dbConfig],
      envFilePath: join(process.cwd(), 'apps/api/.env'),
    }),
    NestEmitterModule.forRoot(new EventEmitter()),
    AuthModule,
    DatabaseModule,
    GraphqlModule,
    ImporterModule,
    UserModule,
    VoterModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
