import { Controller, Get, Post, Request, UseGuards } from '@nestjs/common';

import { Message, LoginResult, ProfileResult } from '@ynamer/api-interfaces';
import { AuthService, JwtAuthGuard, LocalAuthGuard } from '@ynamer/api/auth';

import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly authService: AuthService,
  ) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  public async login(@Request() req): Promise<LoginResult> { // TODO typehint Request from express
    return this.authService.login(req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Post('logout')
  public async logout(@Request() req): Promise<void> { // TODO typehint Request from express
    return this.authService.logout(req.headers.authorization)
  }

  @UseGuards(JwtAuthGuard)
  @Get('profile')
  public async getProfile(@Request() req): Promise<ProfileResult> {
    const { password, ...user } = req.user;

    return user;
  }
}
