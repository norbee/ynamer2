import { registerAs } from '@nestjs/config';
import * as fs from 'fs';

export default registerAs('auth', () => ({
  jwtExpiresIn: process.env.AUTH_JWT_EXPIRES_IN || '60s',
  jwtPrivateKey: fs.readFileSync(
    process.env.AUTH_JWT_PRIVATE_KEY_PATH || './apps/api/private.pem'
  ),
  jwtPublicKey: fs.readFileSync(
    process.env.AUTH_JWT_PUBLIC_KEY_PATH || './apps/api/public.pem'
  ),
  jwtAlgorithm: 'RS256',
}));
