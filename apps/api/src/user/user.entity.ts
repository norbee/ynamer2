import { Column, Entity } from 'typeorm';

import { BaseEntity, GraphqlCompatibility } from '@ynamer/api/database';
import { User as GraphqlUser } from '@ynamer/graphql';

@Entity()
export class User extends BaseEntity implements GraphqlCompatibility<GraphqlUser> {
  @Column({ unique: true })
  public username: string;

  @Column()
  public password?: string;

  @Column()
  public firstName: string;

  @Column()
  public lastName: string;

  @Column({ unique: true })
  public email: string;

  @Column({ default: true })
  public isActive: boolean;

  @Column({ default: false })
  public isAdmin: boolean;

  @Column({ default: null })
  public lastLoginAt?: Date;

  public getGraphqlRepresentation(): GraphqlUser {
    return {
      id: this.id,
      username: this.username,
      firstName: this.firstName,
      lastName: this.lastName,
      email: this.email,
    }
  }
}
