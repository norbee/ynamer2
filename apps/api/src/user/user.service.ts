import { Injectable, Inject, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Transaction, TransactionManager, EntityManager } from 'typeorm';

import { User } from './user.entity';
import { CreateUserDto } from './dto';

@Injectable()
export class UserService {
  public constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @Inject(Logger) private readonly logger: Logger,
  ) {}

  @Transaction()
  public async create(
    createUserDto: CreateUserDto,
    @TransactionManager() entityManager?: EntityManager,
  ): Promise<string> {
    let user = this.userRepository.create(createUserDto);

    try {
      user = await entityManager.save(user);
    }
    catch (e) {
      this.logger.debug(e.message)
      throw new Error('User cannot be created');
    }

    this.logger.debug(`User created: ${user.id}`);

    return user.id;
  }

  public async getOneById(id: string): Promise<User> {
    return this.userRepository.findOneOrFail(id);
  }

  public async findOneById(id: string): Promise<User | null> {
    return this.userRepository.findOne(id);
  }

  public async findOneByUsername(username: string): Promise<User | null> {
    return this.userRepository.findOne({ username });
  }

  public async findAll(): Promise<User[]> {
    return this.userRepository.find();
  }
}
