import { hashSync } from 'bcrypt';

 // todo validate

interface CreateUserAttributes {
  username: string;
  password: string;
  firstName: string;
  lastName: string;
  email: string;
}

export class CreateUserDto {
  public username: string;
  public password: string;
  public firstName: string;
  public lastName: string;
  public email: string;

  public constructor(attributes: CreateUserAttributes) {
    this.username = attributes.username;
    this.firstName = attributes.firstName;
    this.lastName = attributes.lastName;
    this.email = attributes.email;

    this.password = hashSync(attributes.password, 10); // todo saltRounds from config ?
  }
}
