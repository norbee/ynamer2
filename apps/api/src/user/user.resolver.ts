import { NotFoundException, InternalServerErrorException, UseGuards } from '@nestjs/common';
import { Resolver, Query, Args, Mutation } from '@nestjs/graphql';

import { CreateUserInput, User as GraphqlUser } from '@ynamer/graphql';
import { GqlAuthGuard } from '@ynamer/api/auth/guards';

import { UserService } from './user.service';
import { CreateUserDto } from './dto';

@Resolver('User')
export class UserResolver {
  public constructor(
    private readonly userService: UserService,
  ) {}

  @Query()
  @UseGuards(GqlAuthGuard)
  // todo RolesGuard
  public async user(@Args('id') id: string): Promise<GraphqlUser> {
    const user = await this.userService.findOneById(id);
    if (!user) {
      throw new NotFoundException('User not found');
    }

    return user.getGraphqlRepresentation();
  }

  @Query()
  @UseGuards(GqlAuthGuard)
  // todo RolesGuard
  public async users(): Promise<GraphqlUser[]> {
    const users = await this.userService.findAll(); 
    
    return users.map(user => user.getGraphqlRepresentation())
  }

  @Mutation()
  @UseGuards(GqlAuthGuard)
  public async createUser(@Args('input') input: CreateUserInput): Promise<string> {
    const createUserDto = new CreateUserDto(input);

    try {
      return this.userService.create(createUserDto);
    } catch(e) {
      throw new InternalServerErrorException('User cannot be created');
    }
  }
}
