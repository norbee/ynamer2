import { Module, Global, Logger } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { User } from './user.entity';
import { UserService } from './user.service';
import { UserResolver } from './user.resolver';

@Global()
@Module({
  imports: [TypeOrmModule.forFeature([User])],
  providers: [
    Logger,
    UserService,
    UserResolver,
  ],
  exports: [UserService],
})
export class UserModule {}
