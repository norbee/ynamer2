import { Injectable, Inject, Logger, ForbiddenException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { InjectEventEmitter } from 'nest-emitter';
import { Repository, Transaction, TransactionManager, EntityManager } from 'typeorm';

import { AppEventEmitter } from '@ynamer/api/app';
import { UserService } from '@ynamer/api/user';

import { Vote } from '../entities';
import { CreateVoteDto } from '../dto';
import { ChildService } from './child.service';
import { NameService } from './name.service';
import { VoterEvent } from '../voter.events';

@Injectable()
export class VoteService {
  public constructor(
    @InjectEventEmitter() private readonly emitter: AppEventEmitter,
    @InjectRepository(Vote) private readonly voteRepository: Repository<Vote>,
    @Inject(UserService) private readonly userService: UserService,
    @Inject(ChildService) private readonly childService: ChildService,
    @Inject(NameService) private readonly nameService: NameService,
    @Inject(Logger) private readonly logger: Logger,
  ) {
    this.logger.setContext(VoteService.name);
  }

  @Transaction()
  public async create(
    createVoteDto: CreateVoteDto,
    @TransactionManager() entityManager?: EntityManager,
  ) {
    const user = await this.userService.getOneById(createVoteDto.userId);
    const child = await this.childService.getOneById(createVoteDto.childId);
    const name = await this.nameService.getOneById(createVoteDto.nameId);

    if(!child.family.isUserPartOfThisFamily(user)) {
      throw new ForbiddenException('User cannot vote on this child');
    }

    const vote = this.voteRepository.create({ user, child, name, score: createVoteDto.score, updatedAt: new Date() })

    try {
      const result = await entityManager
        .createQueryBuilder()
        .insert()
        .into(Vote)
        .values([vote])
        .orUpdate({ conflict_target: ['user', 'child', 'name'], overwrite: ['id', 'score', 'updatedAt'] })
        .execute();
    }
    catch (e) {
      this.logger.debug(e.message);
      throw new Error('Vote cannot be created');
    }

    this.emitter.emit(VoterEvent.VOTE_CREATED, { userId: user.id, childId: child.id });
    this.logger.debug(`Vote created: ${vote.id}`);

    return vote.id;
  }

  public async find(userId: string, childId: string): Promise<Vote[]> {
    const user = await this.userService.getOneById(userId);
    const child = await this.childService.getOneById(childId);

    return this.voteRepository.find({
      where: { user, child },
      relations: ['user', 'child', 'name'],
    });
  }
}
