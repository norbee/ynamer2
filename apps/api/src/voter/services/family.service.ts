import { Injectable, Inject, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Transaction, TransactionManager, EntityManager } from 'typeorm';

import { User, UserService } from '@ynamer/api/user';

import { Family } from '../entities';
import { CreateFamilyDto } from '../dto';

@Injectable()
export class FamilyService {
  public constructor(
    @InjectRepository(Family) private readonly familyRepository: Repository<Family>,
    @Inject(UserService) private readonly userService: UserService,
    @Inject(Logger) private readonly logger: Logger,
  ) {
    this.logger.setContext(FamilyService.name);
  }

  @Transaction()
  public async create(
    createFamilyDto: CreateFamilyDto,
    @TransactionManager() entityManager?: EntityManager,
  ): Promise<string> {
    const mother = await this.userService.getOneById(createFamilyDto.motherId);
    const father = await this.userService.getOneById(createFamilyDto.fatherId);

    let family = this.familyRepository.create({ mother, father });

    try {
      family = await entityManager.save(family);
    }
    catch (e) {
      this.logger.debug(e.message);
      throw new Error('Family cannot be created');
    }

    this.logger.debug(`Family created: ${family.id}`);

    return family.id;
  }

  public async getOneById(id: string): Promise<Family> {
    return this.familyRepository.findOneOrFail(id);
  }

  public async getOneByUserId(userId: string): Promise<Family> {
    const user = await this.userService.getOneById(userId);
    
    return this.familyRepository.findOneOrFail({
      where: [
        { mother: user },
        { father: user },
      ],
    });
  }
}
