import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Stats } from '../entities';

@Injectable()
export class StatsService {
  public constructor(
    @InjectRepository(Stats) private readonly statsRepository: Repository<Stats>,
  ) {}

  public async getOne(userId: string, childId: string): Promise<Stats> {
    return this.statsRepository.findOneOrFail({ userId, childId });
  }
}
