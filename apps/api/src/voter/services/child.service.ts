import { Injectable, Inject, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Transaction, TransactionManager, EntityManager } from 'typeorm';

import { Child } from '../entities';
import { CreateChildDto } from '../dto';
import { FamilyService } from './family.service';

@Injectable()
export class ChildService {
  public constructor(
    @InjectRepository(Child) private readonly childRepository: Repository<Child>,
    @Inject(FamilyService) private readonly familyService: FamilyService,
    @Inject(Logger) private readonly logger: Logger,
  ) {
    this.logger.setContext(ChildService.name);
  }

  @Transaction()
  public async create(
    createChildDto: CreateChildDto,
    @TransactionManager() entityManager?: EntityManager,
  ): Promise<string> {
    const family = await this.familyService.getOneById(createChildDto.familyId);

    let child = this.childRepository.create({
      ...createChildDto,
      family,
    })

    try {
      child = await entityManager.save(child);
    }
    catch (e) {
      this.logger.debug(e.message);
      throw new Error('Child cannot be created');
    }

    this.logger.debug(`Child created: ${child.id}`);

    return child.id;
  }

  public async getOneById(id: string): Promise<Child> {
    return this.childRepository.findOneOrFail(id);
  }

  public async find(userId: string): Promise<Child[]> {
    const family = await this.familyService.getOneByUserId(userId);

    return this.childRepository.find({
      where: { family }
    })
  }
}
