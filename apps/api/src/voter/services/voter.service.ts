import { Injectable, Inject } from '@nestjs/common';
import { Transaction, TransactionManager, EntityManager } from 'typeorm';

import { UserService } from '@ynamer/api/user';

import { ChildService } from './child.service';
import { SignupDto, CreateFamilyDto, CreateChildDto } from '../dto';
import { FamilyService } from './family.service';

@Injectable()
export class VoterService {
  public constructor(
    @Inject(UserService) private readonly userService: UserService,
    @Inject(ChildService) private readonly childService: ChildService,
    @Inject(FamilyService) private readonly familyService: FamilyService,
  ) {}

  @Transaction()
  public async signup(
    signupDto: SignupDto,
    @TransactionManager() entityManager?: EntityManager,
  ): Promise<string> {
    const motherId = await this.userService.create(signupDto.mother, entityManager);
    const fatherId = await this.userService.create(signupDto.father, entityManager);

    const createFamilyDto = new CreateFamilyDto({ motherId, fatherId});
    const familyId = await this.familyService.create(createFamilyDto, entityManager);

    signupDto.child.familyId = familyId;
    this.childService.create(signupDto.child, entityManager);

    return familyId;
  }
}
