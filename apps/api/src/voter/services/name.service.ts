import { Inject, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Brackets, EntityManager, Repository, Transaction, TransactionManager } from 'typeorm';

import { ChildService } from './child.service';
import { CreateNameDto } from '../dto';
import { Name } from '../entities';
import { Round, Score } from '../enums';

@Injectable()
export class NameService {
  public constructor(
    @InjectRepository(Name) private readonly nameRepository: Repository<Name>,
    @Inject(ChildService) private readonly childService: ChildService,
    @Inject(Logger) private readonly logger: Logger,
  ) {
    this.logger.setContext(NameService.name);
  }

  @Transaction()
  public async create(
    createNameDto: CreateNameDto,
    @TransactionManager() entityManager?: EntityManager,
  ): Promise<string> {
    let name = this.nameRepository.create(createNameDto);

    try {
      name = await entityManager.save(name);
    }
    catch (e) {
      this.logger.debug(e.message);
      throw new Error('Name cannot be created');
    }

    this.logger.debug(`Name created: ${name.name} - ${name.id}`);

    return name.id;
  }

  public async getOneById(id: string): Promise<Name> {
    return this.nameRepository.findOneOrFail(id);
  }

  public async getRandom(childId: string, userId: string, round?: Round): Promise<Name> {
    const child = await this.childService.getOneById(childId);

    return this.nameRepository
      .createQueryBuilder('name')
      .leftJoinAndSelect('name.votes', 'vote', 'vote.childId = :childId AND vote.userId = :userId', { childId, userId })
      .where('name.language = :language', { language: child.language })
      .andWhere('name.gender = :gender', { gender: child.gender })
      .andWhere(new Brackets(qb => {
        if (round === Round.ONE) {
          qb.where('vote.score IS NULL');
        }
        else if (round === Round.TWO) {
          qb.where('vote.score = :whyNot', { whyNot: Score.WHY_NOT });
        }
        else {
          qb.where('vote.score IS NULL').orWhere('vote.score = :whyNot', { whyNot: Score.WHY_NOT });
        }
      }))
      .orderBy('RAND()') // todo check if this can be optimized
      .getOne();
  }
}
