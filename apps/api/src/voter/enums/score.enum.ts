export enum Score {
  NO_WAY = 'NO_WAY',
  WHY_NOT = 'WHY_NOT',
  ONE = 'ONE',
  TWO = 'TWO',
  THREE = 'THREE',
  FOUR = 'FOUR',
  FIVE = 'FIVE',
}
