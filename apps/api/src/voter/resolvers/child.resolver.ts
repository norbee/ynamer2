import { UseGuards } from '@nestjs/common';
import { Resolver, Query, Mutation, Args } from '@nestjs/graphql';

import { CurrentUser, GqlAuthGuard } from '@ynamer/api/auth';
import { User } from '@ynamer/api/user';
import { Child as GraphqlChild, CreateChildInput } from '@ynamer/graphql';

import { CreateChildDto } from '../dto';
import { ChildService, FamilyService } from '../services';

@Resolver('Child')
export class ChildResolver {
  public constructor(
    private readonly childService: ChildService,
    private readonly familyService: FamilyService,
  ) {}

  @Query()
  @UseGuards(GqlAuthGuard)
  public async children(@CurrentUser() user: User): Promise<GraphqlChild[]> {
    const children = await this.childService.find(user.id);

    return children.map(child => child.getGraphqlRepresentation());
  }

  @Mutation()
  @UseGuards(GqlAuthGuard)
  public async createChild(
    @Args('input') input: CreateChildInput,
    @CurrentUser() user: User,
  ) {
    const family = await this.familyService.getOneByUserId(user.id);
    const createChildDto = new CreateChildDto({ ...input, familyId: family.id });

    return this.childService.create(createChildDto);
  }
}
