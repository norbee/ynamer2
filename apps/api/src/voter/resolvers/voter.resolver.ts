import { Resolver, Mutation, Args } from '@nestjs/graphql';

import { SignupInput } from '@ynamer/graphql';
import { CreateUserDto } from '@ynamer/api/user/dto';

import { VoterService } from '../services';
import { CreateChildDto, SignupDto } from '../dto';

@Resolver('Voter')
export class VoterResolver {
  public constructor(
    private readonly voterService: VoterService,
  ) {}

  @Mutation()
  public async signup(@Args('input') input: SignupInput): Promise<string> {
    const signupDto = new SignupDto({
      mother: new CreateUserDto(input.mother),
      father: new CreateUserDto(input.father),
      child: new CreateChildDto({ ...input.child, familyId: 'to-be-created' }),
    })

    return this.voterService.signup(signupDto);
  }
}
