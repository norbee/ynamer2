import { UseGuards, Inject, OnModuleInit } from '@nestjs/common';
import { Resolver, Query, Args, Subscription } from '@nestjs/graphql';
import { PubSub } from 'graphql-subscriptions';
import { InjectEventEmitter } from 'nest-emitter';

import { AppEventEmitter } from '@ynamer/api/app';
import { CurrentUser, GqlAuthGuard } from '@ynamer/api/auth';
import { Channel } from '@ynamer/api/graphql';
import { User } from '@ynamer/api/user';
import { PUB_SUB } from '@ynamer/api/constants';
import {
  FamilyStats as GraphqlFamilyStats,
  Stats as GraphqlStats,
} from '@ynamer/graphql';

import { StatsService, FamilyService } from '../services';
import { VoterEvent } from '../voter.events';

@Resolver('Stats')
export class StatsResolver implements OnModuleInit {
  public constructor(
    private readonly statsService: StatsService,
    private readonly familyService: FamilyService,
    @Inject(PUB_SUB) private readonly pubSub: PubSub,
    @InjectEventEmitter() private readonly emitter: AppEventEmitter,
  ) {}

  public onModuleInit(): void {
    this.emitter.on(VoterEvent.VOTE_CREATED, async payload => {
      this.pubSub.publish(Channel.FAMILY_STATS_UPDATED, {
        [Channel.FAMILY_STATS_UPDATED]: await this.getFamilyStats(payload.userId, payload.childId),
      })
    });
  }

  @Query()
  @UseGuards(GqlAuthGuard)
  public async stats(
    @Args('childId') childId: string,
    @CurrentUser() user: User,
  ): Promise<GraphqlStats> {
    return (await this.statsService.getOne(user.id, childId)).getGraphqlRepresentation();
  }

  @Query()
  @UseGuards(GqlAuthGuard)
  public async familyStats(
    @Args('childId') childId: string,
    @CurrentUser() user: User,
  ): Promise<GraphqlFamilyStats> {
    return this.getFamilyStats(childId, user.id);
  }

  @Subscription(Channel.FAMILY_STATS_UPDATED, {
    filter: (payload, variables, context) => {
      const listener = context.req.user as User;
      const data = payload[Channel.FAMILY_STATS_UPDATED];

      return (
        (
          data.self.userId === listener.id
          || data.partner.userId === listener.id
        )
        && data.self.childId === variables.childId
      );
    },
    resolve: (payload, args, context) => {
      const listener = context.req.user as User;
      const data = payload[Channel.FAMILY_STATS_UPDATED];

      if (listener.id === data.partner.userId) {
        [data.self, data.partner] = [data.partner, data.self];
      }

      return data;
    }
  })
  @UseGuards(GqlAuthGuard)
  public async familyStatsUpdated(): Promise<any> { // todo typehint AsyncIterator<T>
    return this.pubSub.asyncIterator(Channel.FAMILY_STATS_UPDATED);
  }

  private async getFamilyStats(userId: string, childId: string): Promise<GraphqlFamilyStats> {
    const family = await this.familyService.getOneByUserId(userId);
    const partner = family.getPartner(userId);

    return {
      self: (await this.statsService.getOne(userId, childId)).getGraphqlRepresentation(),
      partner: (await this.statsService.getOne(partner.id, childId)).getGraphqlRepresentation(),
    };
  }
}
