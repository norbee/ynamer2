import { NotFoundException, UseGuards } from '@nestjs/common';
import { Args, Resolver, Query } from '@nestjs/graphql';

import { CurrentUser, GqlAuthGuard } from '@ynamer/api/auth';
import { User } from '@ynamer/api/user';
import { RandomName as GraphqlRandomName, Round as GraphqlRound} from '@ynamer/graphql';

import { Round } from '../enums';
import { NameService } from '../services';

@Resolver('Name')
export class NameResolver {
  public constructor(
    private readonly nameService: NameService,
  ) {}

  @Query()
  @UseGuards(GqlAuthGuard)
  public async randomName(
    @Args('childId') childId: string,
    @Args('round') round: GraphqlRound | undefined,
    @CurrentUser() user: User
  ): Promise<GraphqlRandomName> {
    const roundMap = {
      [GraphqlRound.ONE]: Round.ONE,
      [GraphqlRound.TWO]: Round.TWO,
    }

    const name = await this.nameService.getRandom(
      childId,
      user.id,
      round ? roundMap[round] : undefined
    );

    if (!name) {
      throw new NotFoundException('Name not found');
    }

    return {
      name: name.getGraphqlRepresentation(),
      round: !name.votes.length ? Round.ONE : Round.TWO,
    }
  }
}
