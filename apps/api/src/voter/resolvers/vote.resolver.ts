import { UseGuards } from '@nestjs/common';
import { Resolver, Mutation, Args, Query } from '@nestjs/graphql';

import { CurrentUser, GqlAuthGuard } from '@ynamer/api/auth';
import { User } from '@ynamer/api/user';
import { Vote as GraphqlVote, VoteInput } from '@ynamer/graphql';

import { CreateVoteDto } from '../dto';
import { VoteService } from '../services';

@Resolver('Vote')
export class VoteResolver {
  public constructor(
    private readonly voteService: VoteService,
  ) {}

  @Query()
  @UseGuards(GqlAuthGuard)
  public async votes(
    @Args('childId') childId: string,
    @CurrentUser() user: User,
  ): Promise<GraphqlVote[]> {
    return this.voteService.find(user.id, childId);
  }

  @Mutation()
  @UseGuards(GqlAuthGuard)
  public async vote(
    @Args('input') input: VoteInput,
    @CurrentUser() user: User,
  ): Promise<string> {
    const creteVoteDto = new CreateVoteDto({ ...input, userId: user.id })

    return this.voteService.create(creteVoteDto);
  }
}
