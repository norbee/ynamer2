export enum VoterEvent {
  VOTE_CREATED = 'vote-created',
}

export interface VoterEvents {
  [VoterEvent.VOTE_CREATED]: { userId: string, childId: string },
}
