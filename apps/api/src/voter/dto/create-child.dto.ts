import { Gender, Language } from '../enums';

// todo validate

interface CreateChildAttributes {
  familyId: string;
  title: string;
  lastName: string;
  gender: Gender;
  language: Language;
}

export class CreateChildDto {
  public familyId: string;
  public title: string;
  public lastName: string;
  public gender: Gender;
  public language: Language;

  public constructor(attributes: CreateChildAttributes) {
    this.familyId = attributes.familyId;
    this.title = attributes.title;
    this.lastName = attributes.lastName;
    this.gender = attributes.gender;
    this.language = attributes.language;
  }
}
