// todo validate

interface CreateFamilyAttributes {
  motherId: string;
  fatherId: string;
}

export class CreateFamilyDto {
  public motherId: string;
  public fatherId: string;

  public constructor(attributes: CreateFamilyAttributes) {
    this.motherId = attributes.motherId;
    this.fatherId = attributes.fatherId;
  }
}
