import { Gender, Language } from '../enums';

interface CreateNameAttributes {
  gender: Gender;
  language: Language;
  name: string;
}

export class CreateNameDto {
  public gender: Gender;
  public language: Language;
  public name: string;

  public constructor(attributes: CreateNameAttributes) {
    this.gender = attributes.gender;
    this.language = attributes.language;
    this.name = attributes.name;
  }
}
