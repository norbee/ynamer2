import { CreateUserDto } from '@ynamer/api/user/dto';

import { CreateChildDto } from './create-child.dto';

// todo validate

interface SignupAttributes {
  mother: CreateUserDto;
  father: CreateUserDto;
  child: CreateChildDto;
}

export class SignupDto {
  public mother: CreateUserDto;
  public father: CreateUserDto;
  public child: CreateChildDto;

  public constructor(attributes: SignupAttributes) {
    this.mother = attributes.mother;
    this.father = attributes.father;
    this. child = attributes.child;
  }
}
