import { Score } from "../enums";

// todo validate

interface CreateVoteAttributes {
  userId: string;
  childId: string;
  nameId: string;
  score: Score;
}

export class CreateVoteDto {
  public userId: string;
  public childId: string;
  public nameId: string;
  public score: Score;

  public constructor(attributes: CreateVoteAttributes) {
    this.userId = attributes.userId;
    this.childId = attributes.childId;
    this.nameId = attributes.nameId;
    this.score = attributes.score;
  }
}
