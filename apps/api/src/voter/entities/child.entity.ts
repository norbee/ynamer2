import { Column, Entity, ManyToOne, JoinColumn, Index } from 'typeorm';

import { BaseEntity, GraphqlCompatibility } from '@ynamer/api/database';
import { Child as GraphqlChild } from '@ynamer/graphql';

import { Gender, Language } from '../enums';
import { Family } from './family.entity';

@Entity()
export class Child extends BaseEntity implements GraphqlCompatibility<GraphqlChild> {
  @Column()
  public title: string;

  @Column({
    type: 'enum',
    enum: Gender,
  })
  public gender: Gender;

  @Column()
  public lastName: string;

  @ManyToOne(type => Family, { eager: true })
  @JoinColumn()
  @Index()
  public family: Family;

  @Column({
    type: 'enum',
    enum: Language,
    default: Language.HU,
  })
  public language: Language;

  public getGraphqlRepresentation(): GraphqlChild {
    return {
      id: this.id,
      title: this.title,
      gender: this.gender,
      lastName: this.lastName,
      family: this.family.getGraphqlRepresentation(),
      language: this.language,
    }
  }
}
