import { Entity, OneToOne, JoinColumn } from 'typeorm';

import { BaseEntity, GraphqlCompatibility } from '@ynamer/api/database';
import { User } from '@ynamer/api/user';
import { Family as GraphqlFamily } from '@ynamer/graphql';

@Entity()
export class Family extends BaseEntity implements GraphqlCompatibility<GraphqlFamily> {
  @OneToOne(type => User, { eager: true })
  @JoinColumn()
  public mother: User;

  @OneToOne(type => User, { eager: true })
  @JoinColumn()
  public father: User;

  public getGraphqlRepresentation(): GraphqlFamily {
    return {
      id: this.id,
      mother: this.mother.getGraphqlRepresentation(),
      father: this.father.getGraphqlRepresentation(),
    }
  }

  public isUserPartOfThisFamily(user: User): boolean {
     return user.id === this.mother.id || user.id === this.father.id;
  }

  public getPartner(user: User | string): User {
    const userId = user instanceof User ? user.id : user;

    return userId === this.mother.id ? this.father : this.mother;
  }
}
