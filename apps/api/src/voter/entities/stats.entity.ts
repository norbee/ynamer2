import { ViewEntity, ViewColumn } from 'typeorm';

import { GraphqlCompatibility } from '@ynamer/api/database';
import { Stats as GraphqlStats } from '@ynamer/graphql';

@ViewEntity({
  expression: `
    SELECT 
      U.id as userId,
      C.id as childId,
      ifnull(NC.all_names_count, 0) as allNamesCount,
      ifnull(VC.no_way_count, 0) as noWayCount,
      ifnull(VC.why_not_count, 0) as whyNotCount,
      ifnull(VC.ranked_count, 0) as rankedCount,
      ifnull(NC.all_names_count, 0) - ifnull(VC.ranked_count, 0) - ifnull(VC.no_way_count, 0) as leftCount,
      (100 - round(((ifnull(NC.all_names_count, 0) - ifnull(VC.ranked_count, 0) - ifnull(VC.no_way_count, 0)) / ifnull(NC.all_names_count, 0)) * 100)) as progress
    FROM
      user U
    INNER JOIN
      family F ON F.fatherId = U.id OR F.motherId = U.id
    INNER JOIN
      child C ON C.familyId = F.id
    LEFT JOIN
      (
        SELECT
          N.gender,
          N.language,
          count(N.id) as all_names_count
        FROM
          name N
        GROUP BY
          gender, language
      ) NC ON NC.gender = C.gender AND NC.language = C.language
    LEFT JOIN
      (
        SELECT
          V.userId,
          V.childId,
          sum(if(V.score NOT IN ('NO_WAY', 'WHY_NOT'), 1, 0)) as ranked_count,
          sum(if(V.score = 'NO_WAY', 1, 0)) as no_way_count,
          sum(if(V.score = 'WHY_NOT', 1, 0)) as why_not_count
        FROM
          vote V
        GROUP BY
          userId, childId
      ) VC ON VC.userId = U.id AND VC.childId = C.id
  `
})
export class Stats implements GraphqlCompatibility<GraphqlStats> {
  @ViewColumn()
  public userId: string;

  @ViewColumn()
  public childId: string;

  @ViewColumn()
  public allNamesCount: number;

  @ViewColumn()
  public noWayCount: number;

  @ViewColumn()
  public whyNotCount: number;

  @ViewColumn()
  public rankedCount: number;

  @ViewColumn()
  public leftCount: number;

  @ViewColumn()
  public progress: number;

  public getGraphqlRepresentation(): GraphqlStats {
    return {
      userId: this.userId,
      childId: this.childId,
      allNamesCount: this.allNamesCount,
      noWayCount: this.noWayCount,
      whyNotCount: this.whyNotCount,
      rankedCount: this.rankedCount,
      leftCount: this.leftCount,
      progress: this.progress,
    }
  }
}
