import { Entity, Column, Index, OneToMany } from 'typeorm';

import { BaseEntity, GraphqlCompatibility } from '@ynamer/api/database';
import { Name as GraphqlName } from '@ynamer/graphql'

import { Gender, Language } from '../enums';
import { Vote } from './vote.entity';

@Entity()
@Index(['gender', 'language'])
@Index(['gender', 'language', 'name'], { unique: true })
export class Name extends BaseEntity implements GraphqlCompatibility<GraphqlName> {
  @Column({
    type: 'enum',
    enum: Gender,
  })
  public gender: Gender;

  @Column({
    type: 'enum',
    enum: Language,
  })
  public language: Language;

  @Column({
    collation: 'utf8_bin',
  })
  public name: string;

  @OneToMany('Vote', 'name')
  public votes: Vote[];

  public getGraphqlRepresentation(): GraphqlName {
    return {
      id: this.id,
      gender: this.gender,
      language: this.language,
      name: this.name,
    }
  }
}
