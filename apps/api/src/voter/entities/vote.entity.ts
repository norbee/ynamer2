import { Entity, Column, JoinColumn, ManyToOne, Index } from 'typeorm';

import { BaseEntity, GraphqlCompatibility } from '@ynamer/api/database';
import { User } from '@ynamer/api/user';
import { Vote as GraphqlVote } from '@ynamer/graphql';

import { Child } from './child.entity';
import { Name } from './name.entity';
import { Score } from '../enums';

@Entity()
@Index(['user', 'child'])
@Index(['user', 'child', 'name'], { unique: true })
export class Vote extends BaseEntity implements GraphqlCompatibility<GraphqlVote> {
  @ManyToOne(type => User)
  @JoinColumn()
  public user: User;

  @ManyToOne(type => Child)
  @JoinColumn()
  public child: Child;

  @ManyToOne('Name', 'votes')
  public name: Name;

  @Column({
    type: 'enum',
    enum: Score,
  })
  public score?: Score;

  public getGraphqlRepresentation(): GraphqlVote {
    return {
      id: this.id,
      user: this.user.getGraphqlRepresentation(),
      child: this.child.getGraphqlRepresentation(),
      name: this.name.getGraphqlRepresentation(),
      score: this.score,
    }
  }
}
