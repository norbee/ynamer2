import { Module, Logger } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Child, Family, Name, Stats, Vote } from './entities';
import { 
  ChildService,
  FamilyService,
  VoterService,
  NameService,
  VoteService,
  StatsService,
} from './services';
import {
  NameResolver,
  StatsResolver,
  VoterResolver,
  VoteResolver,
  ChildResolver,
} from './resolvers';

@Module({
  imports: [
    TypeOrmModule.forFeature([Child, Family, Name, Stats, Vote])
  ],
  providers: [
    Logger,
    ChildService,
    ChildResolver,
    FamilyService,
    NameService,
    NameResolver,
    StatsService,
    StatsResolver,
    VoteService,
    VoteResolver,
    VoterService,
    VoterResolver,
  ],
  exports: [
    NameService,
  ],
})
export class VoterModule {}
