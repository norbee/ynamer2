export interface GraphqlCompatibility<GraphqlEntity> {
  getGraphqlRepresentation(): GraphqlEntity;
}
