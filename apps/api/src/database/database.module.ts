import { Module, Global } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';

@Global()
@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      useFactory: (config: ConfigService) => {
        const databaseConfig = config.get('database');
        return {
          ...databaseConfig,
          entities: [
              __dirname + '/../**/*.entity{.ts,.js}',
          ],
          autoLoadEntities: true,
          synchronize: true,
        }
      },
      inject: [
        ConfigService,
      ]
    }),
  ],

})
export class DatabaseModule {}
