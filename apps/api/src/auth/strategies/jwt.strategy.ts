import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { JwtPayload } from '@ynamer/api-interfaces';
import { User, UserService } from '@ynamer/api/user';
import { ExtractJwt, Strategy } from 'passport-jwt';

import { AuthService } from '../auth.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  public constructor(
    readonly authService: AuthService,
    readonly configService: ConfigService,
    readonly userService: UserService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get('auth.jwtPublicKey'),
      algorithms: [configService.get('auth.jwtAlgorithm')],
    });
  }

  public async validate(payload: JwtPayload): Promise<User> {
    if (this.authService.isBlackListed(payload)) {
      throw new UnauthorizedException();
    }

    const user = await this.userService.findOneById(payload.sub);
    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
