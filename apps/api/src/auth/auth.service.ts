import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { JwtPayload, LoginResult } from '@ynamer/api-interfaces';
import { User, UserService } from '@ynamer/api/user';

import { compareSync } from 'bcrypt';
import { v4 as uuidV4 } from 'uuid';

@Injectable()
export class AuthService {
  private blackList: Array<{ uid: string; exp: number; }> = [];

  public constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  public async validateUser(username: string, plainTextPassword: string): Promise<User | null> {
    const user = await this.userService.findOneByUsername(username);

    return (user && compareSync(plainTextPassword, user.password)) ? user : null;
  }

  public async login(user: User): Promise<LoginResult> {
    const payload: JwtPayload = {
      uid: uuidV4(),
      sub: user.id,
      username: user.username,
    }

    return {
      access_token: this.jwtService.sign(payload)
    }
  }

  public async logout(authHeader: string): Promise<void> {
    this.addToBlackList(
      this.decodeAuthorizationHeader(authHeader),
    );
  }

  public isBlackListed(jwtPayload: JwtPayload): boolean {
    return !!this.blackList.find(({ uid }) => uid === jwtPayload.uid)
  }

  private decodeAuthorizationHeader(authHeader: string): JwtPayload {
    return this.jwtService.decode(authHeader.split(' ')[1]) as JwtPayload
  }


  private addToBlackList(payload: JwtPayload): void {
    this.cleanupBlacklist()

    this.blackList.push({
      uid: payload.uid,
      exp: payload.exp,
    })
  }

  private cleanupBlacklist(): void {
    this.blackList = this.blackList.filter(({ exp }) => exp && exp > Date.now() / 1000)
  }

}
