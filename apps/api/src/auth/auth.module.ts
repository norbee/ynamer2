import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';

import { AuthService } from './auth.service';
import { JwtStrategy, LocalStrategy } from './strategies';

@Module({
  imports: [
    PassportModule,
    JwtModule.registerAsync({
      useFactory: (config: ConfigService) => {
        const authConfig = config.get('auth');

        return {
          privateKey: authConfig.jwtPrivateKey,
          publicKey: authConfig.jwtPublicKey,
          signOptions: {
            expiresIn: authConfig.jwtExpiresIn,
            algorithm: authConfig.jwtAlgorithm,
          },
        };
      },
      inject: [ConfigService],
    }),
  ],
  providers: [AuthService, JwtStrategy, LocalStrategy],
  exports: [AuthService],
})
export class AuthModule {}
