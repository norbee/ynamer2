import { Module, Global } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { PubSub } from 'graphql-subscriptions';
import { join } from 'path';

import { PUB_SUB } from '@ynamer/api/constants';

const SCHEMAS_INPUT = 'libs/graphql/src/lib/schemas/*.graphql';
const INTERFACES_OUTPUT = 'libs/graphql/src/lib/interfaces.ts';

@Global()
@Module({
  imports: [
    GraphQLModule.forRoot({
      typePaths: [join(process.cwd(), SCHEMAS_INPUT)],
      definitions: {
        path: join(process.cwd(), INTERFACES_OUTPUT),
      },
      installSubscriptionHandlers: true,
      context: ({ req, connection }) => connection ? { req: { headers: connection.context } } : { req },
    }),
  ],
  providers: [
    {
      provide: PUB_SUB,
      useValue: new PubSub(),
    }
  ],
  exports: [PUB_SUB],
})
export class GraphqlModule {}
