import { Module, HttpModule, Logger } from "@nestjs/common";

import { VoterModule } from '@ynamer/api/voter';

import { ImporterService } from './importer.service';

@Module({
  imports: [
    HttpModule,
    VoterModule,
  ],
  providers: [
    Logger,
    ImporterService
  ],
  exports: [ImporterService],
})
export class ImporterModule {}
