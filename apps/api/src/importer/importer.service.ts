import { Injectable, HttpService, Logger } from "@nestjs/common";
import { Observable, from, of } from 'rxjs';
import { map, switchMap, catchError } from 'rxjs/operators';

import { CreateNameDto, NameService } from '@ynamer/api/voter';

import { Source, Sources } from './sources';

@Injectable()
export class ImporterService {
  private stats: string[] = [];

  public constructor(
    private readonly httpService: HttpService,
    private readonly nameService: NameService,
    private readonly logger: Logger,
  ) {
    this.logger.setContext(ImporterService.name);
  }

  public async import(): Promise<void> {
    const promises: Promise<void>[] = [];

    return new Promise(resolve => {
      Sources.forEach(source => {
        promises.push(
          this.storeNames(source, this.fetchNames(source)),
        );
      });

      Promise.all(promises).then(() => {
        this.stats.forEach(stat => this.logger.log(stat));
        this.logger.log('All sources done');
        resolve();
      })
    })
  }

  private async storeNames(source: Source, names: Observable<string>): Promise<void> {
    this.logger.log(`${source.name} - Storing names...`);

    return new Promise(resolve => {
      let skipped = 0;
      let added = 0;
      const promises: Promise<string | void>[] = [];

      names.subscribe({
        next: (name) => {
          name = name.trim();
          if (name.length === 0) {
            return;
          }

          const createNameDto = new CreateNameDto({
            gender: source.gender,
            language: source.language,
            name: name.toLocaleLowerCase(),
          });

          promises.push(
            this.nameService.create(createNameDto)
              .then(() => { added++ })
              .catch(() => { skipped++ })
          );
        },
        complete: () => {
          Promise.all(promises).then(() => {
            this.logger.log(`${source.name} - Done`);
            this.stats.push(`${source.name} - Added: ${added}, Skipped: ${skipped}`);
            resolve();
          })
        }
      });
    })
  }

  private fetchNames(source: Source): Observable<string> {
    this.logger.log(`${source.name} - Fetching names...`);

    return this.httpService
      .get(source.url, { responseType: 'arraybuffer' })
      .pipe(
        map<any, string>(response => 
          response.data.toString(source.encoding)
        ),
        map(data => data.substring(
          data.indexOf(source.startAfter) + source.startAfter.length)
        ),
        switchMap<string, Observable<string>>(data =>
          from(data.split(source.separator))
        ),
        catchError(error =>
          of(`Something went wrong: ${error}`)
        )
      );
  }
}
