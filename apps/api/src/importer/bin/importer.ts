#!/usr/bin/env ts-node

import { NestFactory } from "@nestjs/core";
import { Logger, HttpModule, HttpService } from '@nestjs/common';

import { AppModule } from '@ynamer/api/app/app.module';

import { ImporterService } from '../importer.service';

async function importer() {
  const app = await NestFactory.create(AppModule);
  const logger = app.get(Logger);
  const importerService = app.get(ImporterService);
  
  logger.log('Script STARTED', 'Importer');

  await importerService.import();

  logger.log('Script FINISHED', 'Importer');
  
  await app.close();
}

importer();
