import { Gender, Language } from '@ynamer/api/voter';

export interface Source {
  name: string;
  url: string;
  language: Language;
  gender: Gender;
  encoding: string;
  startAfter: string;
  separator: string;
}

export const Sources: Source[] = [
  {
    name: 'HU-BOY',
    url: 'http://www.nytud.mta.hu/oszt/nyelvmuvelo/utonevek/osszesffi.txt',
    language: Language.HU,
    gender: Gender.BOY,
    encoding: 'latin1',
    startAfter: '-- ',
    separator: '\n',
  },
  {
    name: 'HU-GIRL',
    url: 'http://www.nytud.mta.hu/oszt/nyelvmuvelo/utonevek/osszesnoi.txt',
    language: Language.HU,
    gender: Gender.GIRL,
    encoding: 'latin1',
    startAfter: '-- ',
    separator: '\n',
  },
]
